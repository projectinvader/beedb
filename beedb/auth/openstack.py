from beedb.auth import interface


class OpenStackAuthorization(interface.CloudAuthorization):

    def __init__(self, auth_headers):
        super(OpenStackAuthorization, self).__init__(
            auth_headers)
        self.keystone_credentials = {
            'token': self.headers.get("x-openstack-auth-token"),
            'auth_url': self.headers.get("x-openstack-auth-url"),
            'region_name': self.headers.get("x-openstack-region-name"),
        }

    def authorize(self):
        if 'v2' in self.keystone_credentials['auth_url']:
            from keystoneclient.v2_0 import client
        else:
            from keystoneclient.v3 import client

        keystone = client.Client(**self.keystone_credentials)
        keystone.authenticate()
        return keystone
