import botocore.session

from beedb.auth import interface


class AWSAuthorization(interface.CloudAuthorization):

    def __init__(self, auth_headers):
        super(AWSAuthorization, self).__init__(auth_headers)
        self.aws_credentials = {
            'aws_session_token': self.headers.get('x-aws-session-token'),
            'region_name': self.headers.get('x-aws-region-name'),
            'endpoint_url': self.headers.get('x-aws-endpoint-url')
        }

    def authorize(self):
        session = botocore.session.get_session()
        client = session.create_client('sts', **self.aws_credentials)
        return client
