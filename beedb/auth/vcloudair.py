from pyvcloud import vcloudsession

from beedb.auth import interface
from beedb.common import exceptions


class vCloudAuthorization(interface.CloudAuthorization):

    def __init__(self, auth_headers):
        super(vCloudAuthorization, self).__init__(
            auth_headers)
        self.vcloud_headers = {
            'vcloud_token': self.headers.get('x-vcloud-authorization'),
            'vcloud_org_url': self.headers.get('x-vcloud-org-url'),
            'vcloud_version': self.headers.get('x-vcloud-version'),
        }

    def authorize(self):
        vcloud_org_url = self.vcloud_headers.get('vcloud_org_url')
        vcloud_version = self.vcloud_headers.get('vcloud_version')
        vcloud_token = self.vcloud_headers.get('vcloud_token')
        vcs = vcloudsession.VCS(
            vcloud_org_url, None, None, None,
            vcloud_org_url, vcloud_org_url,
            version=vcloud_version)
        if not vcs.login(token=vcloud_token):
            raise exceptions.BeeDBError("Unauthorized.",
                                        status_code=401)
