class CloudAuthorization(object):

    def __init__(self, auth_headers):
        self.headers = auth_headers

    def authorize(self):
        raise Exception("This methods should be implemented "
                        "for all cloud authorization strategies.")
