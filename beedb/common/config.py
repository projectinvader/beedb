import multiprocessing

import beedb

from oslo_config import cfg
from oslo_utils import importutils


class PortOpt(cfg.Opt):
    def __init__(self, name, **kwargs):
        super(PortOpt, self).__init__(
            name,
            type=cfg.types.Integer(min=80, max=65535),
            **kwargs)


base_configuration = [
    cfg.StrOpt('storage_type',
               default="beedb.storage.postgresql.PostgreSQLStorageV1",
               choices=(
                   'beedb.storage.postgresql.PostgreSQLStorageV1',
               ),
               required=True),
    cfg.IPOpt("bind_host", version=4,
              default="0.0.0.0"),
    PortOpt("bind_port", default=8888),
    cfg.IntOpt("api_workers", default=multiprocessing.cpu_count()),
    cfg.StrOpt('db_uri', default='sqlite:////tmp/beedb_backend.db'),
    cfg.StrOpt('auth_platform',
               default='beedb.auth.openstack.OpenStackAuthorization',
               help="Auth platform inherited from cloud environment",
               choices=('beedb.auth.openstack.OpenStackAuthorization',
                        'beedb.auth.vcloudair.vCloudAuthorization',
                        'beedb.auth.aws.AWSAuthorization',),
               required=True),
    cfg.BoolOpt("debug_mode", default=True,
                help="Enables debug for API service."),

    cfg.StrOpt("secret_storage_platform",
               default="beedb.secrets.vault.SecretStore",
               help="Secrets storage for Roles passwords.",
               choices=("beedb.secrets.plain.SecretStore",
                        "beedb.secrets.vault.SecretStore",)),
]

logging_opts = [
    cfg.StrOpt("level",
               default="DEBUG",
               choices=("DEBUG", "INFO", "WARN", "ERROR")),
    cfg.StrOpt("file", default="/tmp/beedb.log"),
    cfg.StrOpt("formatter",
               default='[%(asctime)s] - '
                       'PID: %(process)s - '
                       '%(name)s - '
                       '%(levelname)s - '
                       '{%(pathname)s:%(lineno)d} - '
                       '%(module)s - '
                       '%(funcName)s - '
                       '%(message)s')
]

logging_group = cfg.OptGroup("logging", "BeeDB logging config.")


vault_opts = [
    cfg.StrOpt("url", help="HashiCorp Vault secrets storage URL.",
               required=True),
    cfg.StrOpt("root_token",
               help="HashiCorp Vault secrets storage root token.",
               required=True,
               secret=True),
    cfg.BoolOpt("enable_ssl_verification", default=False,
                help="SSL/TLS certs verification."),
    cfg.StrOpt("cert", help="SSL/TLS cert to use for verification.",
               default=None),
    cfg.StrOpt("secret_route_template", default="secret/beedb-role-id-{0}")
]
vault_group = cfg.OptGroup("vault", "HashiCorp Vault secrets storage.")


CONF = cfg.CONF

CONF.register_group(logging_group)
CONF.register_opts(base_configuration)
CONF.register_opts(logging_opts, logging_group)

CONF.register_group(vault_group)
CONF.register_opts(vault_opts, vault_group)


def parse_args(argv, default_config_files=None):
    from beedb.common import utils
    cfg.CONF(args=argv[1:],
             project='beedb',
             version=beedb.VERSION,
             default_config_files=default_config_files)
    cfg.CONF.log_opt_values(utils.setup_logging(__name__),
                            utils.get_logging_level())
    importutils.import_class(CONF.auth_platform)
