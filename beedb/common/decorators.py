import flask
import functools


from sqlalchemy import engine
from sqlalchemy import exc
from sqlalchemy import orm

from beedb.common import exceptions
from beedb.common import utils

logger = utils.setup_logging(__name__)


def __service_handler(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            logger.debug("Entering {0} method.".format(
                action.__name__))
            logger.info("Accepting request with parameters: {0}."
                        .format(str(args)))
            result = action(*args, **kwargs)
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            return result
        except exceptions.BeeDBError as ex:
            logger.debug("Workflow failed. Exiting {0} method.".format(
                action.__name__))
            return flask.make_response(str(ex),
                                       ex.status_code)
    return handle


def __partial_dao_handler(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            logger.debug("Entering {0} method.".format(
                action.__name__))
            result = action(*args, **kwargs)
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            return result
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            logger.debug("Workflow failed. Exiting {0} method."
                         .format(action.__name__))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 500
            raise exceptions.BeeDBError(
                message=str(ex),
                status_code=status_code)

    return handle


# TODO(denismakogon): Add URL to pass it into engine
def __execute_query(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            logger.debug("Entering {0} method.".format(
                action.__name__))
            self = args[0]
            query = action(*args, **kwargs)
            db_engine = engine.create_engine(self.admin_url)
            session = orm.sessionmaker(bind=db_engine,
                                       autocommit=True,
                                       autoflush=True)()
            session.connection().connection.set_isolation_level(0)
            output = session.execute(query)
            try:
                output = list(output)
            except exc.ResourceClosedError:
                output = [(), ]
            session.connection().connection.set_isolation_level(1)
            db_engine.dispose()
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            return output
        except (exc.DatabaseError,
                exc.ProgrammingError,
                exc.InternalError) as ex:
            logger.error(str(ex))
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            raise exceptions.BeeDBError(
                message="Internal error. "
                        "Problems with storage. Reason: %s" % str(ex),
                status_code=500)

    return handle


def __create_database_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        logger.debug("Entering {0} method.".format(
            action.__name__))
        self, tenant, database, role, database_type = args
        from beedb.db import model
        try:
            _role = model.TenantRole.find_by(
                tenant=tenant,
                name=role,
                database_type=database_type,
            )
            if not _role:
                raise exceptions.BeeDBError(
                    message=("Role '%s' not found" % role),
                    status_code=404,
                )
            if model.TenantDatabase.find_by(
                tenant=tenant,
                name=database,
                database_type=database_type,
                role=role,
            ):
                raise exceptions.BeeDBError(
                    message=("Database '%s' already exists"
                             % database),
                    status_code=409,
                )
            new_args = list(args[:-1])
            new_args.append(_role.password)
            url = action(*new_args, **kwargs)
            _database = model.TenantDatabase(
                tenant,
                database,
                _role.name,
                description=kwargs.get('description'))
            _database = _database.update(
                status=model.DatabaseStatus.AVAILABLE,
                updated_at=utils.utcnow(),
                role=_role.name)
            _database.save()
            _role.update(status=model.RoleStatus.IN_USE)
            _role.save()
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            _view = _database.to_dict()
            _view.update(url=url)
            return _view
        except Exception as ex:
            logger.error(str(ex))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 500
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            raise exceptions.BeeDBError(
                message=("Unable to create database "
                         "for given tenant. Reason: %s" % str(ex)),
                status_code=status_code)

    return handle


def __get_database_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        logger.debug("Entering {0} method.".format(
            action.__name__))
        self, tenant, database, database_type = args
        try:
            from beedb.db import model
            _database = model.TenantDatabase.find_by(
                tenant=tenant,
                name=database,
                database_type=database_type)
            if _database:
                new_args = self, _database.role, database
                action(*new_args, **kwargs)
                logger.debug("Exiting {0} method.".format(
                    action.__name__))
                _role = model.TenantRole.find_by(
                    name=_database.role,
                    tenant=tenant,
                    database_type=database_type)
                url = self.build_url(_role.name, _role.password, database)
                view = _database.to_dict()
                view.update(url=url)
                return view
            else:
                logger.debug("Exiting {0} method.".format(
                    action.__name__))
                raise exceptions.BeeDBError(
                    message="Database %s was not found." % database,
                    status_code=404)
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            logger.exception(str(ex))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 500
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            raise exceptions.BeeDBError(
                message=("Unable to get database info. "
                         "Reason: %s." % str(ex)),
                status_code=status_code)

    return handle


def __list_databases_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        logger.debug("Entering {0} method.".format(
            action.__name__))
        self, tenant, database_type = args
        try:
            from beedb.db import model
            _databases = []
            _roles = model.TenantRole.get_all_by(
                tenant=tenant,
                database_type=database_type)
            for _role in _roles:
                new_args = self, _role.name
                databases = action(*new_args, **kwargs)
                for d in databases:
                    _database = model.TenantDatabase.find_by(
                        tenant=tenant,
                        name=d,
                        database_type=database_type)
                    url = self.build_url(_role.name, _role.password, d)

                    _dct = _database.to_dict()
                    _dct.update(url=url)
                    _databases.append(_dct)
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            return _databases
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            logger.exception(str(ex))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 500
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            raise exceptions.BeeDBError(
                message=("Unable to list databases. "
                         "Reason: %s." % str(ex)),
                status_code=status_code)

    return handle


def __delete_database_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        logger.debug("Entering {0} method.".format(
            action.__name__))
        self, tenant, database, database_type = args
        from beedb.db import model
        try:
            _database = model.TenantDatabase.find_by(
                tenant=tenant,
                name=database,
                database_type=database_type)
            if not _database:
                raise exceptions.NotFound(
                    database_type=database_type,
                    database=database)
            new_args = self, _database.role, database
            action(*new_args, **kwargs)
            _database.delete()
            _role = model.TenantRole.find_by(
                name=_database.role,
                tenant=tenant,
                database_type=database_type)
            if not _role.has_more_assignments():
                _role.update(status=model.RoleStatus.FREE)
                _role.save()
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            return {}
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            logger.exception(str(ex))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 500
            if 'does not exist' in str(ex):
                _database = model.TenantDatabase.find_by(
                    tenant=tenant,
                    name=database,
                    database_type=database_type)
                _database.delete()
                logger.debug("Exiting {0} method.".format(
                    action.__name__))
                return {}
            logger.debug("Exiting {0} method.".format(
                action.__name__))
            raise exceptions.BeeDBError(
                message=("Unable to delete databases. "
                         "Reason: %s" % str(ex)),
                status_code=status_code)

    return handle


def __create_role_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        from beedb.db import model
        try:
            self, tenant, role, password, database_type = args
            description = kwargs.get('description')
            if model.TenantRole.find_by(name=role):
                raise exceptions.BeeDBError(
                    message="Role %s already exists." % role,
                    status_code=409
                )
            if not model.DatabaseType.find_by(name=database_type):
                raise exceptions.BeeDBError(
                    message="Database type not found.",
                    status_code=404)
            action(*args[:-1], **kwargs)

            _role = model.TenantRole(
                tenant,
                role,
                password,
                database_type,
                description=description
            )
            return _role.to_dict()
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            raise exceptions.BeeDBError(
                message=("Unable to create role. "
                         "Reason: %s" % str(ex)),
                status_code=ex.status_code)
    return handle


def __update_role_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        from beedb.db import model
        try:
            self, tenant, role_name, password, database_type = args
            if not model.TenantRole.find_by(name=role_name):
                raise exceptions.BeeDBError(
                    message="Role %s not found." % role_name,
                    status_code=404
                )
            role = model.TenantRole.find_by(name=role_name)
            if not model.DatabaseType.find_by(name=role.database_type):
                raise exceptions.BeeDBError(
                    message="Database type not found.",
                    status_code=404)
            action(*args[:-1], **kwargs)

            role.update_password(password)

            return role.to_dict()
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            raise exceptions.BeeDBError(
                message=("Unable to update role. "
                         "Reason: %s" % str(ex)),
                status_code=ex.status_code)
    return handle


def __list_roles_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            self, tenant, database_type = args
            from beedb.db import model
            _roles = model.TenantRole.get_all_by(
                tenant=tenant,
                database_type=database_type)
            roles_names = [_r.name for _r in _roles]
            kwargs.update(roles=roles_names)
            roles_mapping = action(*args[:-1], **kwargs)
            for role, exists in roles_mapping.items():
                if not exists:
                    _role = model.TenantRole.find_by(name=role,
                                                     tenant=tenant)
                    _role.update(status=model.RoleStatus.FAILED)
                    _role.save()
            return [r.to_dict() for r in model.TenantRole.get_all_by(
                tenant=tenant,
                database_type=database_type)]
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            raise exceptions.BeeDBError(
                message=("Unable to list roles. "
                         "Reason: %s" % str(ex)),
                status_code=ex.status_code)
    return handle


def __get_role_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            self, tenant, role = args
            from beedb.db import model
            _role = model.TenantRole.find_by(tenant=tenant,
                                             name=role)
            exists = action(*args, **kwargs)
            if _role and exists:
                return _role.to_dict()
            else:
                raise exceptions.BeeDBError(
                    message="Role %s not found." % role,
                    status_code=404
                )
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            raise exceptions.BeeDBError(
                message=("Unable to get role. "
                         "Reason: %s" % str(ex)),
                status_code=ex.status_code)
    return handle


def __delete_role_workflow(action):
    @functools.wraps(action)
    def handle(*args, **kwargs):
        try:
            self, tenant, role = args
            from beedb.db import model
            _role = model.TenantRole.find_by(
                tenant=tenant,
                name=role,
            )
            if _role:
                if _role.status == model.RoleStatus.IN_USE:
                    raise exceptions.BeeDBError(
                        message="Unable to delete role %s "
                                "because it is still in use"
                                % role,
                        status_code=403)
                action(*args, **kwargs)
                _role.delete()
                return {}
            else:
                raise exceptions.BeeDBError(
                    message="Role %s not found for tenant %s."
                            % (role, tenant),
                    status_code=404
                )
        except (Exception, exc.DBAPIError,
                exceptions.BeeDBError) as ex:
            raise exceptions.BeeDBError(
                message=("Unable to delete role. "
                         "Reason: %s" % str(ex)),
                status_code=ex.status_code)
    return handle

service_handler = functools.partial(__service_handler)
dao_handler = functools.partial(__partial_dao_handler)
validate_request_data = functools.partial(__partial_dao_handler)
execute_query = functools.partial(__execute_query)
create_database_workflow = functools.partial(__create_database_workflow)
get_database_workflow = functools.partial(__get_database_workflow)
list_databases_workflow = functools.partial(__list_databases_workflow)
delete_database_workflow = functools.partial(__delete_database_workflow)
create_role_workflow = functools.partial(__create_role_workflow)
update_role_workflow = functools.partial(__update_role_workflow)
list_roles_workflow = functools.partial(__list_roles_workflow)
get_role_workflow = functools.partial(__get_role_workflow)
delete_role_workflow = functools.partial(__delete_role_workflow)

__all__ = [
    'service_handler',
    'dao_handler',
    'validate_request_data',
    'execute_query',
    'create_database_workflow',
    'get_database_workflow',
    'list_databases_workflow',
    'delete_database_workflow',
    'create_role_workflow',
    'update_role_workflow',
    'list_roles_workflow',
    'get_role_workflow',
    'delete_role_workflow',
]
