import re

from beedb.common import utils
from beedb.common import i18n

_FATAL_EXCEPTION_FORMAT_ERRORS = False

logger = utils.setup_logging(__name__)


def safe_fmt_string(text):
    return re.sub(r'%([0-9]+)', r'\1', text)


class _BaseBeeDBException(Exception):
    """
    Base Exception

    To correctly use this class, inherit from it and define
    a 'message' property. That message will get printf'd
    with the keyword arguments provided to the constructor.
    """
    message = "An unknown exception occurred"

    def __init__(self, **kwargs):
        try:
            self._error_string = self.message % kwargs

        except Exception as e:
            if _FATAL_EXCEPTION_FORMAT_ERRORS:
                raise e
            else:
                # at least get the core message out if something happened
                self._error_string = self.message

    def __str__(self):
        return self._error_string


class BeeDBError(_BaseBeeDBException):
    """Base exception that all custom trove app exceptions inherit from."""
    status_code = 400

    def __init__(self, message=None, status_code=None, **kwargs):
        if message is not None:
            self.message = message
        self.message = safe_fmt_string(self.message)
        if status_code:
            self.status_code = status_code
        logger.exception(i18n._LE(self.message))
        super(BeeDBError, self).__init__(**kwargs)


class NotFound(BeeDBError):
    status_code = 404
    message = "%(database_type)s database: %(database)s not found."
