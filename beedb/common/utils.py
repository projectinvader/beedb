import datetime
import logging
import jsonschema
import uuid

from flask import request
from flask import make_response
from functools import wraps
from werkzeug.exceptions import BadRequest

from beedb.common import config

CONF = config.CONF


def get_logging_level():
    logging_mapping = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
    }
    return logging_mapping.get(CONF.logging.level)


def common_log_setup():
    log_file_handler = logging.FileHandler(CONF.logging.file)
    formatter = logging.Formatter(CONF.logging.formatter,
                                  "%Y-%m-%d %H:%M:%S")
    log_file_handler.setFormatter(formatter)
    return log_file_handler


def setup_logging_for_app(app):
    app.logger.addHandler(common_log_setup())
    app.logger.setLevel(get_logging_level())


def setup_logging(name):
    log_file_handler = common_log_setup()
    logger = logging.getLogger(name)
    logger.addHandler(log_file_handler)
    logger.setLevel(get_logging_level())
    sql_logger = logging.getLogger('sqlalchemy.engine')
    sql_logger.setLevel(get_logging_level())
    sql_logger.addHandler(log_file_handler)
    return logger


def validate_json(schema):
    def wrapper(fn):
        @wraps(fn)
        def decorated(*args, **kwargs):
            try:
                json = request.get_json(force=True)
                jsonschema.validate(json, schema)
                return fn(args[0], json, **kwargs)
            except BadRequest as e:
                setup_logging(__name__).exception(e)
                return make_response("Bad json data in request body."
                                     " Can't parse input json file", 400)
            except jsonschema.ValidationError as e:
                setup_logging(__name__).exception(e)
                return make_response(" Validation error: {}.".format(e),
                                     400)
        return decorated
    return wrapper


def generate_database_password():
    return str(uuid.uuid4())


def utcnow():
    return datetime.datetime.utcnow()
