import testtools

from beedb.common import exceptions
from beedb.storage import postgresql

from beedb.tests.storage.postgresql import common


class PostgreSQLDriverV1TestSuite(common.
                                  PostgreSQLEngineTestCommon):

    def setUp(self):
        super(PostgreSQLDriverV1TestSuite, self).setUp()
        self.driver = postgresql.PostgreSQLStorageV1(
            **self.connection_opts)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        db = self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)
        empty_dict = self.driver.delete_database(
            self.tenant_id,
            self.database,
            self.database_type)
        self.driver.delete_role(self.tenant_id, role['name'])

        self.assertIsNotNone(db)
        self.assertIsInstance(db, dict)
        self.assertIsInstance(empty_dict, dict)
        self.assertEqual(0, len(empty_dict))

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database_no_role(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.create_database,
                               self.tenant_id,
                               self.database,
                               'name',
                               self.database_type,
                               description=self.database)
        self.assertIn("Role '%s' not found" % 'name', str(ex))
        self.assertEqual(404, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database_and_list(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        db = self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)
        dbs = self.driver.list_databases(self.tenant_id,
                                         self.database_type)
        self.driver.delete_database(self.tenant_id, self.database,
                                    self.database_type)
        self.driver.delete_role(self.tenant_id, role['name'])
        self.assertEqual(1, len(dbs))
        self.assertEqual(db['name'], self.database)
        self.assertEqual(db['tenant'], self.tenant_id)
        self.assertEqual(db['role'], dbs[0]['role'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_get(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        db = self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)
        db_clone = self.driver.get_database(
            db['tenant'], db['name'], self.database_type)
        self.driver.delete_database(self.tenant_id, self.database,
                                    self.database_type)
        self.driver.delete_role(self.tenant_id, role['name'])
        self.assertEqual(db['name'], db_clone['name'])
        self.assertEqual(db['tenant'], db_clone['tenant'])
        self.assertEqual(db['role'], db_clone['role'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_duplicate_database(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        db = self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.create_database,
                               db['tenant'],
                               db['name'],
                               role['name'],
                               self.database_type)
        self.assertIn("already exist", str(ex))
        self.assertEqual(409, ex.status_code)
        self.driver.delete_database(self.tenant_id, self.database,
                                    self.database_type,)
        self.driver.delete_role(db['tenant'], role['name'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_get_missing_database(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.get_database,
                               self.tenant_id,
                               self.database,
                               self.database_type)
        self.assertEqual(404, ex.status_code)
        self.assertIn('not found', str(ex))

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_wrong_database(self):
        role_a = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        role_b = self.driver.create_role(
            self.another_tenant,
            self.another_tenant,
            self.password,
            self.database_type
        )
        db = self.driver.create_database(
            self.tenant_id,
            self.database,
            role_a['name'],
            self.database_type,
            description=self.database)
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.delete_database,
                               self.another_tenant,
                               self.database,
                               self.database_type)
        self.assertIn("not found", str(ex))
        self.assertEqual(404, ex.status_code)
        self.driver.delete_database(self.tenant_id, self.database,
                                    self.database_type)
        self.driver.delete_role(db['tenant'], role_a['name'])
        self.driver.delete_role(self.another_tenant, role_b['name'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_role(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        self.assertIsNotNone(role)
        self.assertIsInstance(role, dict)
        self.assertEqual(role['tenant'], self.tenant_id)
        self.assertEqual(role['name'], self.tenant_id)
        self.driver.delete_role(self.tenant_id, role['name'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_update_role(self):
        role = self.driver.create_role(self.tenant_id,
                                       self.tenant_id,
                                       self.password,
                                       self.database_type)
        self.assertIsNotNone(role)
        self.assertIsInstance(role, dict)
        self.assertEqual(role['tenant'], self.tenant_id)
        self.assertEqual(role['name'], self.tenant_id)

        new_password = 'foobar'
        self.driver.update_role(self.tenant_id,
                                self.tenant_id,
                                new_password)

        role_clone = self.driver.get_role(self.tenant_id,
                                          self.tenant_id)
        self.assertEqual(role_clone['password'], new_password)

        role_uri = ("postgresql://%(username)s:%(password)s@"
                    "%(host)s:%(port)s/%(database)s") % {
            'username': role['name'],
            'password': new_password,
            'host': self.connection_opts['host'],
            'port': self.connection_opts['port'],
            'database': self.connection_opts['database']}

        connectivity = common.able_connect_to_database(role_uri)
        self.assertEqual(True, connectivity)

        self.driver.delete_role(self.tenant_id, role['name'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_role_no_database_type(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.create_role,
                               self.tenant_id,
                               self.tenant_id,
                               self.password,
                               "missing database type")
        self.assertIn("Database type", str(ex))

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_role_duplicate(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.create_role,
                               self.tenant_id,
                               self.tenant_id,
                               self.password,
                               self.database_type,
                               )
        self.driver.delete_role(self.tenant_id, role['name'])

        self.assertIsNotNone(role)
        self.assertIsInstance(role, dict)
        self.assertEqual(role['tenant'], self.tenant_id)
        self.assertEqual(role['name'], self.tenant_id)
        self.assertIn("exists", str(ex))
        self.assertEqual(409, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_list_roles(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        role_1 = self.driver.create_role(
            self.tenant_id,
            self.another_tenant,
            self.password,
            self.database_type
        )
        roles = self.driver.list_roles(self.tenant_id,
                                       self.database_type)
        self.driver.delete_role(self.tenant_id, role['name'])
        self.driver.delete_role(self.tenant_id, role_1['name'])
        self.assertIn(role, roles)
        self.assertIn(role_1, roles)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_get_role(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        role_clone = self.driver.get_role(self.tenant_id,
                                          self.tenant_id)
        self.driver.delete_role(self.tenant_id, role['name'])
        self.assertEqual(role, role_clone)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_get_missing_role(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.get_role,
                               self.tenant_id,
                               self.tenant_id)
        self.assertIn("not found", str(ex))
        self.assertEqual(404, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_wrong_role(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.delete_role,
                               self.tenant_id,
                               self.tenant_id)
        self.assertIn("not found", str(ex))
        self.assertEqual(404, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_same_role_with_different_tenants(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.create_role,
                               self.another_tenant,
                               self.tenant_id,
                               self.password,
                               self.database_type)
        self.assertIn("exists", str(ex))
        self.driver.delete_role(self.tenant_id,
                                role['name'])
        self.assertEqual(409, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_role_owned_by_other_tenant(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.delete_role,
                               self.another_tenant,
                               self.tenant_id)
        self.driver.delete_role(self.tenant_id,
                                role['name'])
        self.assertIn('not found for tenant', str(ex))
        self.assertEqual(404, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_role_in_use_state(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)

        ex = self.assertRaises(exceptions.BeeDBError,
                               self.driver.delete_role,
                               self.tenant_id,
                               role['name'])
        self.driver.delete_database(self.tenant_id,
                                    self.database,
                                    self.database_type)
        self.driver.delete_role(self.tenant_id, role['name'])
        self.assertIn("in use", str(ex))
        self.assertEqual(403, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_role_status_updated(self):
        role = self.driver.create_role(
            self.tenant_id,
            self.tenant_id,
            self.password,
            self.database_type
        )
        self.driver.create_database(
            self.tenant_id,
            self.database,
            role['name'],
            self.database_type,
            description=self.database)
        role_clone = self.driver.get_role(role['tenant'],
                                          role['name'])
        self.driver.delete_database(self.tenant_id, self.database,
                                    self.database_type)
        role_clone_2 = self.driver.get_role(role['tenant'],
                                            role['name'])
        self.driver.delete_role(role['tenant'], role['name'])
        self.assertEqual("IN USE", role_clone['status'])
        self.assertEqual("AVAILABLE", role_clone_2['status'])
