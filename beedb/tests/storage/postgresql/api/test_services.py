import testtools

from beedb.application import _common
from beedb.api.regular.databases import service as db_service
from beedb.api.regular.roles import service as role_service

from beedb.tests.storage.postgresql import common


class RoleDatabaseAPIServiceTest(common.PostgreSQLEngineTestCommon):

    def setUp(self):
        super(RoleDatabaseAPIServiceTest, self).setUp()
        self.dbs_service = db_service.DatabasesV1()
        self.db_service = db_service.DatabaseV1()
        self.roles_service = role_service.RolesV1()
        self.role_service = role_service.RoleV1()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_services(self):
        self.assertIsNotNone(self.dbs_service)
        self.assertIsNotNone(self.db_service)
        self.assertIsNotNone(self.roles_service)
        self.assertIsNotNone(self.role_service)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_http_get_databases_no_role(self):
        with _common.app.app_context():
            data, status_code = self.dbs_service.get("lol")
            self.assertEqual({'databases': []}, data)
            self.assertEqual(200, status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_http_get_database_no_role(self):
        with _common.app.app_context():
            response = self.db_service.get("lol", self.database)
            self.assertEqual(404, response.status_code)
            self.assertIn('not found', response.data)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_http_get_roles(self):
        with _common.app.app_context():
            data, status_code = self.roles_service.get(self.tenant_id)
            self.assertEqual({'roles': []}, data)
            self.assertEqual(200, status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_http_get_role(self):
        with _common.app.app_context():
            response = self.role_service.get(self.tenant_id, 'role')
            self.assertEqual(404, response.status_code)
            self.assertIn('not found', response.data)
