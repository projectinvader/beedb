import testtools

from beedb.common import exceptions
from beedb.api.regular.databases import model as db_model
from beedb.api.regular.database_types import model as db_types_model
from beedb.api.regular.roles import model as role_model

from beedb.tests.storage.postgresql import common


class RoleDatabaseAPIModelTest(common.PostgreSQLEngineTestCommon):

    def setUp(self):
        super(RoleDatabaseAPIModelTest, self).setUp()
        self.database_model = db_model.DatabaseModel()
        self.role_model = role_model.RoleModel()
        self.db_types = db_types_model.DatabaseTypeModel()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database(self):
        role = self.role_model.create(self.tenant_id,
                                      self.tenant_id,
                                      self.password,
                                      self.database_type)
        db = self.database_model.create(
            self.tenant_id, self.database,
            role['name'],
            description=self.database)
        self.database_model.delete(self.tenant_id, self.database)
        self.role_model.delete(self.tenant_id, role['name'])

        self.assertIsNotNone(db)
        self.assertIsInstance(db, dict)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database_and_list(self):
        role = self.role_model.create(self.tenant_id,
                                      self.tenant_id,
                                      self.password,
                                      self.database_type)
        db = self.database_model.create(
            self.tenant_id, self.database, role['name'],
            description=self.database)
        dbs = self.database_model.list(self.tenant_id)
        self.database_model.delete(self.tenant_id, self.database)
        self.role_model.delete(self.tenant_id, role['name'])
        self.assertIn(db['name'], [d['name'] for d in dbs])
        self.assertIn(db['tenant'], [d['tenant'] for d in dbs])
        self.assertIn(db['role'], [d['role'] for d in dbs])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_get(self):
        role = self.role_model.create(self.tenant_id,
                                      self.tenant_id,
                                      self.password,
                                      self.database_type)
        db = self.database_model.create(
            self.tenant_id, self.database, role['name'],
            description=self.database)
        db_clone = self.database_model.get(self.tenant_id, self.database)
        self.database_model.delete(self.tenant_id, self.database)
        self.role_model.delete(self.tenant_id, role['name'])
        self.assertEqual(db['name'], db_clone['name'])
        self.assertEqual(db['role'], db_clone['role'])
        self.assertEqual(db['tenant'], db_clone['tenant'])

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_duplicate_database(self):
        role = self.role_model.create(self.tenant_id,
                                      self.tenant_id,
                                      self.password,
                                      self.database_type)
        self.database_model.create(
            self.tenant_id, self.database, role['name'],
            description=self.database)
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.database_model.create,
                               self.tenant_id, self.database,
                               role['name'],
                               description=self.database)
        self.database_model.delete(self.tenant_id, self.database)
        self.role_model.delete(self.tenant_id, role['name'])
        self.assertIsNotNone(ex)
        self.assertIn('already exist', str(ex))
        self.assertEqual(409, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_get_missing_database(self):
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.database_model.get,
                               self.tenant_id,
                               'random_db')
        self.assertIsNotNone(ex)
        self.assertIn('not found', str(ex))
        self.assertEqual(404, ex.status_code)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_wrong_database(self):
        role = self.role_model.create(self.tenant_id,
                                      self.tenant_id,
                                      self.password,
                                      self.database_type)
        self.database_model.create(
            self.tenant_id, self.database, role['name'],
            description=self.database)
        ex = self.assertRaises(exceptions.BeeDBError,
                               self.database_model.delete,
                               self.another_tenant,
                               self.database)
        self.database_model.delete(self.tenant_id, self.database)
        self.role_model.delete(self.tenant_id, role['name'])
        self.assertIn('not found', str(ex))
        self.assertEqual(404, ex.status_code)
