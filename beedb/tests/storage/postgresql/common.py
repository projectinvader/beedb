import os
import testtools
import uuid
import tempfile

from flask.ext import migrate
from sqlalchemy import engine
from sqlalchemy import orm

from beedb.application import _common
from beedb.db import model


admin_user = os.environ.get('TEST_POSTGRESQL_SUPERUSER', '')
admin_pass = os.environ.get('TEST_POSTGRESQL_SUPERUSER_PASSWD', '')
host = os.environ.get('TEST_POSTGRESQL_HOST', '')
port = os.environ.get('TEST_POSTGRESQL_PORT', '')
admin_database = os.environ.get('TEST_POSTGRESQL_DATABASE', '')

secret_store_platform = os.environ.get("TEST_SECRET_STORE_PLATFORM",
                                       'beedb.secrets.plain.SecretStore')
vault_url = os.environ.get("TEST_VAULT_URL")
vault_root_token = os.environ.get("TEST_VAULT_ROOT_TOKEN")

skip_reason = ("One of necessary options wasn't set:\n"
               "Admin database: %s\n"
               "Admin password: %s\n"
               "Admin username: %s\n"
               "PostgreSQL host: %s\n"
               "PostgreSQL port: %s." % (admin_database,
                                         admin_pass,
                                         admin_user,
                                         host,
                                         port))
admin_uri = ("postgresql://%(username)s:%(password)s@"
             "%(host)s:%(port)s/%(database)s") % {
    'username': admin_user,
    'password': admin_pass,
    'host': host,
    'port': port,
    'database': admin_database
}


def able_connect_to_database(uri):
    try:
        _engine = engine.create_engine(uri)
        session = orm.sessionmaker(bind=_engine,
                                   autocommit=True,
                                   autoflush=True)()
        session.connection().close()
        _engine.dispose()
        return True
    except Exception as ex:
        print("Unable to connect to the database using URI: %s. "
              "Reason: %s." % (uri, str(ex)))
        return False

necessary_attrs_set = [admin_database, admin_pass,
                       admin_user, host, port,
                       able_connect_to_database(admin_uri)]


class PostgreSQLEngineTestCommon(testtools.TestCase):

    def setUp(self):
        _common.app.config['TESTING'] = True
        self.db_fd, self.db_fpath = tempfile.mkstemp()
        _common.app.config['SQLALCHEMY_DATABASE_URI'] = (
            "sqlite:///%s.db" % self.db_fpath)
        self.current_dir = os.path.dirname(os.path.realpath(__file__))
        self.tenant_id = str(uuid.uuid4()).replace("-", "")
        self.another_tenant = str(uuid.uuid4()).replace("-", "")
        self.password = 'test'
        self.database = 'test%s' % str(uuid.uuid4()).replace("-", "")
        self.database_type = 'postgresql'

        with _common.app.app_context():
            migrate_dir = self.current_dir + '/../../../../migrations/'
            migrate.upgrade(directory=migrate_dir)
            model.base.db.create_all()

        with _common.app.app_context():
            model.DatabaseType(
                self.database_type,
                admin_user,
                admin_pass,
                admin_database,
                host,
                port,
                'beedb.storage.postgresql.PostgreSQLStorageV1',
                description="testing purposes"
            )
        self.connection_opts = {
            'username': admin_user,
            'password': admin_pass,
            'database': admin_database,
            'host': host,
            'port': port,
        }

        model.CONF.secret_storage_platform = secret_store_platform
        if 'vault' in secret_store_platform:
            model.CONF.vault.url = vault_url
            model.CONF.vault.root_token = vault_root_token

        super(PostgreSQLEngineTestCommon, self).setUp()

    def test_create_database(self):
        pass

    def test_create_database_no_role(self):
        pass

    def test_create_database_and_list(self):
        pass

    def test_create_and_get(self):
        pass

    def test_create_duplicate_database(self):
        pass

    def test_get_missing_database(self):
        pass

    def test_delete_wrong_database(self):
        pass

    def test_create_role(self):
        pass

    def test_create_role_duplicate(self):
        pass

    def test_create_and_list_roles(self):
        pass

    def test_create_and_get_role(self):
        pass

    def test_get_missing_role(self):
        pass

    def test_delete_wrong_role(self):
        pass

    def test_create_same_role_with_different_tenants(self):
        pass

    def test_delete_role_owned_by_other_tenant(self):
        pass

    def test_delete_role_in_use_state(self):
        pass

    def tearDown(self):
        model.CONF.secret_storage_platform = None
        if 'vault' in secret_store_platform:
            model.CONF.vault.url = None
            model.CONF.vault.root_token = None

        os.close(self.db_fd)
        os.unlink(self.db_fpath)
        os.remove("%s.db" % self.db_fpath)

        super(PostgreSQLEngineTestCommon, self).tearDown()
