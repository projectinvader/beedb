import testtools
import uuid

from beedb.common import exceptions
from beedb.storage import postgresql

from beedb.tests.storage.postgresql import common


class PostgreSQLEngineTestSuite(common.
                                PostgreSQLEngineTestCommon):

    def setUp(self):
        super(PostgreSQLEngineTestSuite, self).setUp()
        self._engine = postgresql.PostgreSQLWorkflows(
            self.tenant_id,
            self.password,
            self.database,
            self.connection_opts,
            description=self.database)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database(self):
        self._engine.create_tenant_in_postgresql()
        url = self._engine.create_database()
        self.assertEqual(
            True,
            common.
            able_connect_to_database(url))
        self._engine.delete_database()
        self._engine.delete_role()
        self.assertEqual(False,
                         common.
                         able_connect_to_database(url))

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_database_and_list(self):
        self._engine.create_tenant_in_postgresql()
        self._engine.create_database()
        dbs = self._engine.list_databases()
        self._engine.delete_database()
        self._engine.delete_role()

        self.assertIn(self.database, dbs)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_get(self):
        self._engine.create_tenant_in_postgresql()
        self._engine.create_database()
        exists = self._engine.get_database()
        self.assertEqual(True, exists)
        self._engine.delete_database()
        self._engine.delete_role()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_duplicate_database(self):
        self._engine.create_tenant_in_postgresql()
        self._engine.create_database()
        ex = self.assertRaises(exceptions.BeeDBError,
                               self._engine.create_database)
        self.assertIn("already exist", str(ex))
        self._engine.delete_database()
        self._engine.delete_role()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_delete_wrong_database(self):
        self._engine.create_tenant_in_postgresql()
        self._engine.create_database()
        tenant_engine_2 = postgresql.PostgreSQLWorkflows(
            str(uuid.uuid4()).replace("-", ""),
            self.password,
            "test2",
            self.connection_opts,
            description="test2")
        tenant_engine_2.create_tenant_in_postgresql()
        tenant_engine_2.database = self._engine.database
        ex = self.assertRaises(exceptions.BeeDBError,
                               tenant_engine_2.delete_database)
        self.assertIn("Ownership conflict", str(ex))
        self.assertEqual(403, ex.status_code)
        self._engine.delete_database()
        self._engine.delete_role()
        tenant_engine_2.delete_role()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_get_missing_database(self):
        exist = self._engine.get_database()
        self.assertEqual(False, exist)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_role(self):
        self._engine.create_tenant_in_postgresql()
        true_false = self._engine.get_role()
        self.assertEqual(True, true_false)
        self._engine.delete_role()

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_role_duplicate(self):
        self._engine.create_tenant_in_postgresql()
        true_false = self._engine.get_role()
        ex = self.assertRaises(exceptions.BeeDBError,
                               self._engine.create_tenant_in_postgresql,
                               )
        self._engine.delete_role()
        self.assertIsNotNone(ex)
        self.assertEqual(True, true_false)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_list_roles(self):
        self._engine.create_tenant_in_postgresql()
        roles = self._engine.list_roles([self.tenant_id])
        self._engine.delete_role()
        for role, exists in roles.items():
            self.assertEqual(True, exists)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_create_and_get_role(self):
        self._engine.create_tenant_in_postgresql()
        role = self._engine.get_role()
        self._engine.delete_role()
        self.assertEqual(True, role)

    @testtools.skipIf(not any(common.necessary_attrs_set),
                      common.skip_reason)
    def test_get_missing_role(self):
        role = self._engine.get_role()
        self.assertEqual(False, role)
