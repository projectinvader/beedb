#!/usr/bin/env bash

# PostgreSQL opts
export TEST_POSTGRESQL_SUPERUSER=postgres
export TEST_POSTGRESQL_DATABASE=postgres
export TEST_POSTGRESQL_HOST=10.23.12.221
export TEST_POSTGRESQL_SUPERUSER_PASSWD=postgres
export TEST_POSTGRESQL_PORT=5432

# HashiCorp Vault
export TEST_SECRET_STORE_PLATFORM="beedb.secrets.vault.SecretStore"
export TEST_VAULT_URL=http://192.168.0.111:8200
export TEST_VAULT_ROOT_TOKEN=bb885248-b5d4-b56f-7a45-b0e8eb5f1c4b
