from hvac import v1

from beedb.common import config
from beedb.secrets import interface

CONF = config.CONF


class SecretStore(interface.SecretStore):

    def vault(self):
        host = CONF.vault.url
        root_token = CONF.vault.root_token
        cert = CONF.vault.cert
        verify = CONF.vault.enable_ssl_verification
        client = v1.Client(url=host,
                           token=root_token,
                           verify=verify,
                           cert=cert)
        return client

    def get_secret(self, role_name):
        client = self.vault()
        route = CONF.vault.secret_route_template.format(role_name)
        secret = client.read(route)
        return secret['data']['password']

    def write_secret(self, role_name, password=None):
        route = CONF.vault.secret_route_template.format(role_name)
        client = self.vault()
        client.write(route, password=password)
        client.read(route)
        return route

    def delete_secret(self, role_name):
        client = self.vault()
        route = CONF.vault.secret_route_template.format(role_name)
        client.delete(route)

    def alter_secret(self, role_name, password=None):
        return self.write_secret(role_name, password=password)
