from beedb.secrets import interface


class SecretStore(interface.SecretStore):

    def get_secret(self, role_name):
        from beedb.db import model
        role = model.TenantRole.find_by(name=role_name)
        return role.secret_reference

    def write_secret(self, role_name, password=None):
        return password

    def delete_secret(self, role_name):
        pass

    def alter_secret(self, role_name, password=None):
        return password
