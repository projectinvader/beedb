import sys

from flask.ext import script
from flask.ext import migrate

from beedb.application import regular
from beedb.common import config
from beedb.common import print_utils

CONF = config.CONF

DatabaseTypes = script.Manager(
    usage="Performs action related to Database types")


@DatabaseTypes.option('--name', dest='name',
                      help="Database type name")
@DatabaseTypes.option('--dba-username', dest='dba_username',
                      help="DBA username")
@DatabaseTypes.option('--dba-password', dest='dba_password',
                      help="DBA password")
@DatabaseTypes.option('--db-host', dest='db_host',
                      help="DB host")
@DatabaseTypes.option('--db-port', dest='db_port',
                      help="DB port")
@DatabaseTypes.option('--dba-db', dest='dba_db',
                      help="DBA DB")
@DatabaseTypes.option('--description', dest='description')
@DatabaseTypes.option('--driver', dest='driver',
                      help='Database type driver')
def create(name, dba_username,
           dba_password, dba_db,
           db_host, db_port,
           driver,
           description=None):
    from beedb.db import model
    if not any([db_host, db_port, dba_db,
                dba_password, dba_username, name, driver]):
        raise Exception("Please specify all database type attributes")
    _type = model.DatabaseType(name,
                               dba_username,
                               dba_password,
                               dba_db,
                               db_host,
                               db_port,
                               driver,
                               description=description)
    print_utils.print_dict(_type.to_dict())


@DatabaseTypes.option('--name', dest='name',
                      help="Database type name")
def delete(name):
    if not name:
        raise Exception("Name was not specified.")
    from beedb.db import model
    model.DatabaseType.find_by(name=name).delete()
    print("OK. Done.")


@DatabaseTypes.option('--all', default=None)
def list(all):
    from beedb.db import model
    for _type in model.DatabaseType.list():
        print_utils.print_dict(_type.to_dict())


def main():
    configs = ['beedb-manage', ]
    files = []
    for attr in sys.argv:
        if (attr.startswith('--config-file') or
                attr.startswith('--config-dir')):
            configs.append(attr)
            _pos = sys.argv.index(attr)
            configs.append(sys.argv[_pos + 1])
            _def = sys.argv[_pos]
            _val = sys.argv[_pos + 1]
            files.append(_val)
            sys.argv.remove(_def)
            sys.argv.remove(_val)
    config.parse_args(configs,
                      default_config_files=files)
    flask_app = regular.app
    flask_app.config[
        'SQLALCHEMY_DATABASE_URI'
    ] = CONF.db_uri
    manager = script.Manager(flask_app)

    manager.add_command('db', migrate.MigrateCommand)
    manager.add_command('database-types', DatabaseTypes)

    manager.run()

if __name__ == "__main__":
    main()
