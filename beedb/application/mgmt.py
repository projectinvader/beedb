from beedb.application import _common
from beedb.api.management import register
from beedb.common import utils

register.setup_resources(_common.api)
app = _common.app
db = _common.db
logger = utils.setup_logging(__name__)


def main():
    return _common.main(app)


if __name__ == "__main__":
    main()
