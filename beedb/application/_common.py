import sys
import flask

from flask.ext import restful
from flask.ext import sqlalchemy
from flask.ext import migrate

from beedb.common import config
from beedb.common import utils

app = flask.Flask(__name__)
api = restful.Api(app)
db = sqlalchemy.SQLAlchemy(app)
migrate = migrate.Migrate(app, db)

CONF = config.CONF
logger = utils.setup_logging(__name__)
app.url_map.strict_slashes = False


def main(application):
    config.parse_args(sys.argv)
    utils.setup_logging_for_app(app)
    host, port, workers, debug = (
        CONF.bind_host,
        CONF.bind_port,
        CONF.api_workers,
        CONF.debug_mode,
    )
    app.config[
        'SQLALCHEMY_DATABASE_URI'
    ] = CONF.db_uri
    try:
        application.run(
            host=host,
            port=port,
            processes=workers,
            debug=debug
        )
    except Exception as e:
        print(str(e))
