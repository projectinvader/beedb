import flask

from oslo_utils import importutils

from beedb.application import _common
from beedb.api.regular import register
from beedb.common import config
from beedb.common import exceptions
from beedb.common import utils
from beedb.common import i18n

CONF = config.CONF
app = _common.app
db = _common.db
logger = utils.setup_logging(__name__)


@app.before_request
def authorize():
    logger.debug("Request headers: {0}".format(
        str(flask.request.headers)))
    auth_platform = flask.request.headers.get(
        'x-beedb-auth-platform', 'unknown')
    # TODO(denismakogon): make BeeDB work with multiple
    #                     auth platforms at the same time
    if auth_platform not in CONF.auth_platform:
        logger.error(i18n._LE(
            "Unknown auth platform used. "
            "Auth platform {0} is not supported."
            .format(auth_platform)))
        return flask.make_response(
            "Auth platform {0} is not supported."
            .format(auth_platform), 403)
    else:
        try:
            auth_class = importutils.import_class(CONF.auth_platform)
            auth_platform_instance = auth_class(
                flask.request.headers)
            auth_platform_instance.authorize()
        except (exceptions.BeeDBError, Exception) as ex:
            logger.exception(i18n._LE(str(ex)))
            status_code = ex.status_code if hasattr(
                ex, 'status_code') else 401
            return flask.make_response(str(ex), status_code)


register.setup_resources(_common.api)


def main():
    return _common.main(app)

if __name__ == "__main__":
    main()
