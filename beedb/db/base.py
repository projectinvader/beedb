import uuid

from beedb.application import _common
from beedb.common import utils

db = _common.db
logger = utils.setup_logging(__name__)


class BaseDatabaseModel(object):

    def __init__(self):
        self.id = str(uuid.uuid4())
        self.created_at = utils.utcnow()
        self.updated_at = utils.utcnow()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            logger.error(str(e))
            raise e

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as e:
            logger.error(str(e))
            raise e

    def update(self, **values):
        try:
            for key in values:
                if hasattr(self, key):
                    setattr(self, key, values[key])
            self.updated_at = utils.utcnow()
            self.save()
            return self.find_by(id=self.id)
        except Exception as e:
            logger.error(str(e))
            raise e

    @classmethod
    def list(cls):
        try:
            return cls.query.all()
        except Exception as e:
            logger.error(str(e))
            raise e

    @classmethod
    def find_by(cls, **kwargs):
        obj = cls.query.filter_by(**kwargs).first()
        return obj if obj else None

    @classmethod
    def get_all_by(cls, **kwargs):
        try:
            objs = cls.query.filter_by(**kwargs).all()
            return objs if objs else []
        except Exception as e:
            logger.error(str(e))
            raise e

    def to_dict(self):
        return self.__dict__
