from oslo_utils import importutils

from beedb.db import base
from beedb.common import config

CONF = config.CONF


class DatabaseStatus(object):
    AVAILABLE = "AVAILABLE"
    BUILDING = "BUILDING"
    FAILED = "FAILED"


class TenantDatabase(base.BaseDatabaseModel, base.db.Model):
    __tablename__ = 'tenant_database'

    id = base.db.Column(base.db.String(), primary_key=True)
    tenant = base.db.Column(base.db.String())
    name = base.db.Column(base.db.String(), unique=True)
    description = base.db.Column(base.db.String())
    created_at = base.db.Column(base.db.String())
    updated_at = base.db.Column(base.db.String())
    status = base.db.Column(base.db.String())
    database_type = base.db.Column(base.db.String())
    role = base.db.Column(base.db.String())

    def __init__(self, tenant, database,
                 role, description=None):
        """
        Creates database entity record
        :param tenant: Cloud user unique identified
        :param database: database name
        :param database_type: type of database
                              that is being created
        :param role: Role for database assignment
        :param description: database description
        :return:
        """

        self.name = database
        self.tenant = tenant
        self.description = (description if description
                            else "General purpose database")
        self.status = DatabaseStatus.BUILDING
        self.role = role
        _role = TenantRole.find_by(name=self.role,
                                   tenant=self.tenant)
        self.database_type = DatabaseType.find_by(
            name=_role.database_type).name

        super(TenantDatabase, self).__init__()
        self.save()

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "tenant": self.tenant,
            "description": self.description,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "status": self.status,
            "role": self.role,
            "database_type": self.database_type,
        }


class RoleStatus(object):
    IN_USE = "IN USE"
    FREE = "AVAILABLE"
    FAILED = "FAILED"


class TenantRole(base.BaseDatabaseModel, base.db.Model):
    __tablename__ = 'tenant_role'
    id = base.db.Column(base.db.String())
    tenant = base.db.Column(base.db.String())
    name = base.db.Column(base.db.String(), primary_key=True)
    description = base.db.Column(base.db.String())
    created_at = base.db.Column(base.db.String())
    updated_at = base.db.Column(base.db.String())
    status = base.db.Column(base.db.String())
    database_type = base.db.Column(base.db.String())
    secret_reference = base.db.Column(base.db.String())
    __table_args__ = (
        base.db.PrimaryKeyConstraint(name),
    )

    def __init__(self, tenant, name, password,
                 database_type, description=None):
        self.tenant = tenant
        self.name = name
        self.secret_reference = self._write_password(password)
        self.database_type = database_type
        self.description = (description if description else
                            "general purpose role")
        self.status = RoleStatus.FREE
        super(TenantRole, self).__init__()
        self.save()

    def has_more_assignments(self):
        assigned_db = TenantDatabase.get_all_by(role=self.name)
        return True if len(assigned_db) != 0 else False

    @property
    def password(self):
        return self._read_password()

    def _read_password(self):
        secret_store = importutils.import_class(
            CONF.secret_storage_platform)()
        return secret_store.get_secret(self.name)

    def _write_password(self, password):
        secret_store = importutils.import_class(
            CONF.secret_storage_platform)()
        return secret_store.write_secret(
            self.name,
            password=password)

    def update_password(self, password):
        secret_store = importutils.import_class(
            CONF.secret_storage_platform)()
        return secret_store.alter_secret(self.name, password=password)

    def delete(self):
        secret_store = importutils.import_class(
            CONF.secret_storage_platform)()
        secret_store.delete_secret(self.name)
        super(TenantRole, self).delete()

    def to_dict(self):
        return {
            'id': self.id,
            'tenant': self.tenant,
            'name': self.name,
            'password': self.password,
            'database_type': self.database_type,
            'description': self.description,
            'status': self.status,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }


class DatabaseType(base.BaseDatabaseModel, base.db.Model):
    __tablename__ = 'database_type'
    id = base.db.Column(base.db.String())
    name = base.db.Column(base.db.String(), primary_key=True)
    description = base.db.Column(base.db.String())
    created_at = base.db.Column(base.db.String())
    updated_at = base.db.Column(base.db.String())
    username = base.db.Column(base.db.String())
    password = base.db.Column(base.db.String())
    database = base.db.Column(base.db.String())
    host = base.db.Column(base.db.String())
    port = base.db.Column(base.db.String())
    driver = base.db.Column(base.db.String())
    __table_args__ = (
        base.db.PrimaryKeyConstraint(name),
    )

    def __init__(self, name,
                 dba_username,
                 dba_password, dba_db,
                 db_host, db_port,
                 driver,
                 description=None):
        self.name = name
        self.username = dba_username
        self.password = dba_password
        self.database = dba_db
        self.host = db_host
        self.port = db_port
        self.description = description
        self.driver = driver
        super(DatabaseType, self).__init__()
        self.save()

    def to_dict(self):
        return {
            'name': self.name,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'username': self.username,
            'password': self.password,
            'database': self.database,
            'host': self.host,
            'port': self.port,
            'driver': self.driver,
            'description': self.description
        }

    def get_connection_opts(self):
        return {
            'username': self.username,
            'password': self.password,
            'database': self.database,
            'host': self.host,
            'port': self.port,
        }
