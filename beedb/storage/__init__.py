from oslo_utils import importutils


def storage_connector(driver, **connection_opts):
    return importutils.import_class(driver)(**connection_opts)
