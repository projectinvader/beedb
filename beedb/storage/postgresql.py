from beedb.common import decorators
from beedb.common import exceptions
from beedb.common import utils
from beedb.storage import interface


logger = utils.setup_logging(__name__)


class PostgreSQLWorkflows(interface.StorageSQLWorkflows):

    _admin_url = None

    @property
    def url_template(self):
        return ("postgresql://%(username)s:%(password)s@"
                "%(host)s:%(port)s/%(database)s")

    def build_url(self):
        return self.url_template % {
            'username': self.username,
            'password': self.password,
            'host': self.connection_opts.get('host'),
            'port': self.connection_opts.get('port'),
            'database': self.database
        }

    @property
    def admin_url(self):
        _admin_url = self.url_template % self.connection_opts
        return self._admin_url if self._admin_url else _admin_url

    _create_tanant_role = ('CREATE ROLE "%(username)s" '
                           "NOSUPERUSER NOCREATEDB "
                           "NOCREATEROLE NOINHERIT "
                           "LOGIN ENCRYPTED PASSWORD "
                           "'%(password)s';")

    _update_role = ('ALTER ROLE "%(username)s" '
                    "NOSUPERUSER NOCREATEDB "
                    "NOCREATEROLE NOINHERIT "
                    "LOGIN ENCRYPTED PASSWORD "
                    "'%(password)s';")

    _create_database = ('CREATE DATABASE %(database)s '
                        'WITH OWNER="%(username)s";')

    _comment_timestamp = ("COMMENT ON DATABASE %(database)s IS "
                          "'Database create %(timestamp)s';")

    _comment_purpose = ("COMMENT ON DATABASE %(database)s IS "
                        "'Purpose: %(description)s';")

    _drop_database = "DROP DATABASE %(database)s;"

    _get_databases = ("SELECT datname FROM pg_database "
                      "JOIN pg_authid ON pg_database.datdba = "
                      "pg_authid.oid WHERE rolname='%(username)s';")

    _drop_role = 'DROP ROLE "%(username)s";'

    __get_role = ("WITH RECURSIVE cte AS "
                  "(SELECT oid FROM pg_roles "
                  "WHERE rolname = '%(username)s' UNION ALL "
                  "SELECT m.roleid FROM cte "
                  "JOIN pg_auth_members m "
                  "ON m.member = cte.oid) "
                  "SELECT oid FROM cte;")

    def __init__(self,
                 tenant,
                 password,
                 database,
                 connection_opts,
                 description=None):
        self.username = str(tenant)
        self.password = (utils.generate_database_password()
                         if not password else password)
        self.database = database
        self.description = description
        self.connection_opts = connection_opts

    @decorators.execute_query
    def _get_role(self):
        return self.__get_role % {
            'username': self.username
        }

    @decorators.execute_query
    def create_tenant_in_postgresql(self):
        return self._create_tanant_role % {
            'username': self.username,
            'password': self.password
        }

    @decorators.execute_query
    def update_role(self):
        return self._update_role % {
            'username': self.username,
            'password': self.password
        }

    @decorators.execute_query
    def create_database_for_user(self):
        return self._create_database % {
            'username': self.username,
            'database': self.database
        }

    @decorators.execute_query
    def do_comment_timestamp_in_postgresql(self):
        return self._comment_timestamp % {
            'database': self.database,
            'timestamp': utils.utcnow()
        }

    @decorators.execute_query
    def do_comment_for_database_purpose(self):
        return self._comment_purpose % {
            'database': self.database,
            'description': (self.description if self.description
                            else "General purpose database."),
        }

    @decorators.execute_query
    def delete_role(self):
        return self._drop_role % {
            'username': self.username
        }

    @decorators.execute_query
    def _delete_database(self):
        return self._drop_database % {
            'database': self.database
        }

    def create_database(self):
        """
        Creates database in PostgreSQL.
        This method does next workflows:
        1. Creates database owned by a user with given username
        2. Comments a timestamp.
        3. Comments purpose.
        4. Builds database URL.

        :return: url: new database connection URL
        :rtype: basestring
        """
        try:
            self.create_database_for_user()
        except exceptions.BeeDBError as ex:
            if 'already exists' in str(ex):
                logger.info(
                    "Tenant %s attempted "
                    "create already existing "
                    "database." % self.username)
                ex.status_code = 409
            raise ex
        self.do_comment_timestamp_in_postgresql()
        self.do_comment_for_database_purpose()

        url = self.url_template % {
            'username': self.username,
            'password': self.password,
            'host': self.connection_opts.get('host'),
            'port': self.connection_opts.get('port'),
            'database': self.database
        }
        return url

    @decorators.execute_query
    def get_databases(self):
        return self._get_databases % {
            'username': self.username
        }

    def list_databases(self):
        dbs = self.get_databases()
        return list(dbs[0] if len(dbs) >= 1 else [])

    def get_database(self):
        return (True if self.database in
                self.list_databases() else False)

    def delete_database(self):
        if not self.get_database():
            raise exceptions.BeeDBError(
                message="Unable to delete given database: %s. "
                        "Ownership conflict.",
                status_code=403)
        return self._delete_database()

    def list_roles(self, stored_roles):
        result = {}
        for role in stored_roles:
            self.username = role
            result.update({role: True if len(
                self._get_role()[0]) > 0 else False})
        return result

    def get_role(self):
        return (True if len(
            self._get_role()) > 0
            else False)


class PostgreSQLStorageV1(interface.DatabaseStorageInterface,
                          interface.RoleStorageInterface):

    database_type = "PostgreSQL"

    def build_url(self, role, password, database):
        engine = PostgreSQLWorkflows(role,
                                     password,
                                     database,
                                     self.connection_opts)
        return engine.build_url()

    @decorators.create_database_workflow
    def create_database(self, tenant, database,
                        role, password,
                        description=None):
        engine = PostgreSQLWorkflows(role,
                                     password,
                                     database,
                                     self.connection_opts,
                                     description=description)
        return engine.create_database()

    @decorators.list_databases_workflow
    def list_databases(self, role):
        engine = PostgreSQLWorkflows(role,
                                     None,
                                     None,
                                     self.connection_opts)
        return engine.list_databases()

    @decorators.get_database_workflow
    def get_database(self, tenant, database):
        engine = PostgreSQLWorkflows(tenant,
                                     None,
                                     database,
                                     self.connection_opts)
        return engine.get_database()

    @decorators.delete_database_workflow
    def delete_database(self, tenant, database):
        engine = PostgreSQLWorkflows(tenant,
                                     None,
                                     database,
                                     self.connection_opts)
        engine.delete_database()

    @decorators.create_role_workflow
    def create_role(self, tenant, role_name,
                    password, description=None):
        engine = PostgreSQLWorkflows(role_name,
                                     password,
                                     None,
                                     self.connection_opts,
                                     description=description)
        engine.create_tenant_in_postgresql()

    @decorators.update_role_workflow
    def update_role(self, tenant, role_name, password):
        engine = PostgreSQLWorkflows(role_name,
                                     password,
                                     None,
                                     self.connection_opts)
        engine.update_role()

    @decorators.list_roles_workflow
    def list_roles(self, tenant, roles=None):
        engine = PostgreSQLWorkflows(None,
                                     None,
                                     None,
                                     self.connection_opts)
        return engine.list_roles(roles)

    @decorators.get_role_workflow
    def get_role(self, tenant, role_name):
        engine = PostgreSQLWorkflows(role_name,
                                     None,
                                     None,
                                     self.connection_opts)
        return engine.get_role()

    @decorators.delete_role_workflow
    def delete_role(self, tenant, role_name):
        engine = PostgreSQLWorkflows(role_name,
                                     None,
                                     None,
                                     self.connection_opts)
        engine.delete_role()
