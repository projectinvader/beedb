from beedb.common import exceptions


class StorageSQLWorkflows(object):
    @property
    def admin_url(self):
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    @property
    def url_template(self):
        raise ("Should be implemented within each "
               "database storage interface implementation.")


class DatabaseStorageInterface(object):

    def __init__(self, **connection_opts):
        self.connection_opts = connection_opts

    def build_url(self, role, password, database):
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    def create_database(self, tenant, database,
                        role, password, description=None):
        """
        Creates database with given name and description
        :param database: Database name
        :type database: basestring
        :param description: Database description (optional)
        :type description: basestring
        :param password: role password
        :type password: basestring
        :param role: role to assign database
        :type role: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    def get_database(self, tenant, database):
        """
        Retrieves requested database by given name
        :param database: Database name
        :type database: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    def list_databases(self, tenant):
        """
        Retrieves all database
        :param database: Database name
        :type database: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    def delete_database(self, tenant, database):
        """
        Deletes database with given name and description
        :param database: Database name
        :type database: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    # BeeDB v2
    def restore_database(self, tenant, database):
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")

    def backup_database(self, tenant, database):
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "database storage interface implementation.")


class RoleStorageInterface(object):

    def __init__(self, **connection_opts):
        self.connection_opts = connection_opts

    def create_role(self, tenant, role_name,
                    password, description=None):
        """
        Creates role with given name and description
        :param role_name: Role name
        :type role_name: basestring
        :param description: Role description (optional)
        :type description: basestring
        :param password: Role password for database
        :type password: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "role storage interface implementation.")

    def update_role(self, tenant, role_name, password):
        """
        Updates role with given name and description
        :param role_name: Role name
        :type role_name: basestring
        :param description: Role description (optional)
        :type description: basestring
        :param password: Role password for database
        :type password: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "role storage interface implementation.")

    def get_role(self, tenant, role_name):
        """
        Retrieves requested role by given name
        :param database: Database name
        :type database: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "role storage interface implementation.")

    def list_roles(self, tenant, stored_roles):
        """
        Retrieves all role for tenant
        :param tenant: tenant ID
        :type tenant: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "role storage interface implementation.")

    def delete_role(self, tenant, role_name):
        """
        Deletes role with given name and description
        :param tenant: tenant ID
        :type tenant: basestring
        :param role_name: role name
        :type role_name: basestring
        :return:
        """
        raise exceptions.BeeDBError(
            "Should be implemented within each "
            "role storage interface implementation.")
