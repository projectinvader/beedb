from beedb.api.regular.databases import service as db_service
from beedb.api.regular.database_types import service as db_types_service
from beedb.api.regular.roles import service as role_service


def setup_resources(api):
    # Databases
    api.add_resource(db_service.DatabasesV1,
                     '/v1/<string:tenant>/databases')
    api.add_resource(db_service.DatabaseV1,
                     '/v1/<string:tenant>/databases/'
                     '<string:database>')
    # Roles
    api.add_resource(role_service.RolesV1,
                     '/v1/<string:tenant>/roles')
    api.add_resource(role_service.RoleV1,
                     '/v1/<string:tenant>/roles/'
                     '<string:role>')
    # Database types
    api.add_resource(db_types_service.DatabaseTypes,
                     '/v1/<string:tenant>/databases/types')
