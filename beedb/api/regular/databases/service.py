import json
import flask

from flask.ext import restful

from beedb.common import decorators
from beedb.api.regular.databases import model
from beedb.api.regular.databases import view


class DatabasesV1(restful.Resource):

    @decorators.service_handler
    def get(self, tenant):
        _data = model.DatabaseModel().list(tenant)
        _view = view.DatabasesView(_data).view()
        return _view, 200

    @decorators.service_handler
    def post(self, tenant):
        request_data = json.loads(flask.request.data)
        _data = model.DatabaseModel().create(
            tenant,
            request_data['database'],
            request_data['role'],
            description=request_data.get('description')
        )
        _view = view.DatabaseView(_data).view()
        return _view, 202


class DatabaseV1(restful.Resource):

    @decorators.service_handler
    def get(self, tenant, database):
        _data = model.DatabaseModel().get(tenant, database)
        _view = view.DatabaseView(_data).view()
        return _view, 200

    @decorators.service_handler
    def delete(self, tenant, database):
        model.DatabaseModel().delete(tenant, database)
        return {'database': {}}, 202
