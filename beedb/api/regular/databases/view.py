class DatabaseView(object):

    database_attributes = [
        'id',
        'name',
        'database_type',
        'created_at',
        'tenant',
        'status',
        'url',
        'updated_at',
    ]

    def __init__(self, database_json_repr):
        self.data = database_json_repr

    def build_single_view(self, data):
        view = {'database': {}}
        for attr in self.database_attributes:
            view['database'].update({attr: data.get(attr)})
        return view

    def view(self):
        return self.build_single_view(self.data)


class DatabasesView(DatabaseView):

    def __init__(self, databases):
        super(DatabasesView, self).__init__(databases)

    def view(self):
        view = {'databases': []}
        for chunk in self.data:
            view['databases'].append(self.build_single_view(
                chunk)['database'])
        return view
