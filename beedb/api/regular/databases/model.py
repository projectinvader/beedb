from beedb import storage
from beedb.common import decorators
from beedb.common import exceptions


class DatabaseModel(object):

    @decorators.dao_handler
    def create(self, tenant, database,
               role, description=None):
        from beedb.db import model
        _role = model.TenantRole.find_by(tenant=tenant,
                                         name=role)
        if _role:
            _type = model.DatabaseType.find_by(name=_role.database_type)
            if _type:
                return storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).create_database(
                    tenant,
                    database,
                    role,
                    _type.name,
                    description=description
                )
            else:
                raise exceptions.BeeDBError(
                    "Database type not found", status_code=404)
        else:
            raise exceptions.BeeDBError(
                "Role not found", status_code=404)

    @decorators.dao_handler
    def list(self, tenant):
        from beedb.db import model
        _dbs = model.TenantDatabase.get_all_by(tenant=tenant)
        dbs = []
        for db in _dbs:
            _role = model.TenantRole.find_by(name=db.role)
            _type = model.DatabaseType.find_by(name=_role.database_type)
            dbs.extend(storage.storage_connector(
                _type.driver, **_type.get_connection_opts()
            ).list_databases(tenant, _role.database_type))
        return dbs

    @decorators.dao_handler
    def get(self, tenant, database):
        from beedb.db import model
        db = model.TenantDatabase.find_by(name=database)
        if db:
            _role = model.TenantRole.find_by(name=db.role)
            if _role:
                _type = model.DatabaseType.find_by(name=_role.database_type)
                return storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).get_database(tenant, database, _type.name)
            else:
                raise exceptions.BeeDBError("Role for database not found.",
                                            status_code=404)
        else:
            raise exceptions.BeeDBError("Database not found.",
                                        status_code=404)

    @decorators.dao_handler
    def delete(self, tenant, database):
        self.get(tenant, database)
        from beedb.db import model
        db = model.TenantDatabase.find_by(name=database)
        _role = model.TenantRole.find_by(name=db.role)
        _type = model.DatabaseType.find_by(name=_role.database_type)
        return storage.storage_connector(
            _type.driver, **_type.get_connection_opts()
        ).delete_database(tenant, database, _role.database_type)
