from flask.ext import restful

from beedb.common import decorators
from beedb.api.regular.database_types import model
from beedb.api.regular.database_types import view


class DatabaseTypes(restful.Resource):

    @decorators.service_handler
    def get(self, tenant):
        data = model.DatabaseTypeModel().get()
        _view = view.DatabaseTypesView(data).view()
        return _view, 200
