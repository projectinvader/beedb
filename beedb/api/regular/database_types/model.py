from beedb.common import decorators


class DatabaseTypeModel(object):

    @decorators.dao_handler
    def get(self):
        from beedb.db import model
        return [_type.to_dict()
                for _type in model.DatabaseType.list()]
