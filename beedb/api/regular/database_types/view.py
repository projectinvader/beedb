class DatabaseTypeView(object):

    database_attributes = [
        'name',
        'description',
        'created_at',
        'updated_at',
    ]

    def __init__(self, database_types_json_repr):
        self.data = database_types_json_repr

    def build_single_view(self, data):
        view = {'database_type': {}}
        for attr in self.database_attributes:
            view['database_type'].update({attr: data.get(attr)})
        return view

    def view(self):
        return self.build_single_view(self.data)


class DatabaseTypesView(DatabaseTypeView):

    def __init__(self, database_types):
        super(DatabaseTypesView, self).__init__(database_types)

    def view(self):
        view = {'database_types': []}
        for chunk in self.data:
            view['database_types'].append(self.build_single_view(
                chunk)['database_type'])
        return view
