import json
import flask

from flask.ext import restful

from beedb.common import decorators
from beedb.api.regular.roles import model
from beedb.api.regular.roles import view


class RolesV1(restful.Resource):

    @decorators.service_handler
    def get(self, tenant):
        _data = model.RoleModel().list(tenant)
        _view = view.RolesView(_data).view()
        return _view, 200

    @decorators.service_handler
    def post(self, tenant):
        request_data = json.loads(flask.request.data)
        _data = model.RoleModel().create(
            tenant,
            request_data['name'],
            request_data['password'],
            request_data['database_type'],
            description=request_data['description']
        )
        _view = view.RoleView(_data).view()
        return _view, 202


class RoleV1(restful.Resource):

    @decorators.service_handler
    def get(self, tenant, role):
        _data = model.RoleModel().get(tenant, role)
        _view = view.RoleView(_data).view()
        return _view, 200

    @decorators.service_handler
    def post(self, tenant, role):

        request_data = json.loads(flask.request.data)
        _data = model.RoleModel().update(tenant, role,
                                         request_data['password'])
        _view = view.RoleView(_data).view()
        return _view, 200

    @decorators.service_handler
    def delete(self, tenant, role):
        model.RoleModel().delete(tenant, role)
        return {'role': {}}, 202
