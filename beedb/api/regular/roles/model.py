from beedb import storage
from beedb.common import decorators
from beedb.common import exceptions


class RoleModel(object):

    @decorators.dao_handler
    def create(self, tenant, role_name, role_password,
               database_type,
               description=None):
        from beedb.db import model
        _type = model.DatabaseType.find_by(name=database_type)
        if _type:
            return storage.storage_connector(
                _type.driver, **_type.get_connection_opts()
            ).create_role(
                tenant,
                role_name,
                role_password,
                _type.name,
                description=description
            )
        else:
            raise exceptions.BeeDBError(
                "Database type not found.",
                status_code=404)

    @decorators.dao_handler
    def update(self, tenant, role_name, role_password):
        from beedb.db import model
        _role = model.TenantRole.find_by(name=role_name,
                                         tenant=tenant)
        if _role:
            _type = model.DatabaseType.find_by(name=_role.database_type)
            if _type:
                return storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).update_role(
                    tenant,
                    role_name,
                    role_password,
                    _type.name,
                )
        else:
            raise exceptions.BeeDBError(
                "Database type not found.",
                status_code=404)

    @decorators.dao_handler
    def list(self, tenant):
        roles = []
        from beedb.db import model
        for _role in model.TenantRole.get_all_by(tenant=tenant):
            _type = model.DatabaseType.find_by(name=_role.database_type)
            roles.extend(storage.storage_connector(
                _type.driver, **_type.get_connection_opts()
            ).list_roles(tenant, _type.name))
        return roles

    @decorators.dao_handler
    def get(self, tenant, role_name):
        from beedb.db import model
        _role = model.TenantRole.find_by(name=role_name,
                                         tenant=tenant)
        if _role:
            _type = model.DatabaseType.find_by(name=_role.database_type)
            if _type:
                return storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).get_role(
                    tenant,
                    role_name
                )
            else:
                raise exceptions.BeeDBError(
                    "Database type not found.",
                    status_code=404)
        else:
            raise exceptions.BeeDBError("Role not found.",
                                        status_code=404)

    @decorators.dao_handler
    def delete(self, tenant, role_name):
        from beedb.db import model
        _role = model.TenantRole.find_by(name=role_name,
                                         tenant=tenant)
        if _role:
            _type = model.DatabaseType.find_by(name=_role.database_type)
            if _type:
                storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).get_role(
                    tenant, role_name
                )
                return storage.storage_connector(
                    _type.driver, **_type.get_connection_opts()
                ).delete_role(
                    tenant, role_name
                )
        else:
            raise exceptions.BeeDBError("Role not found.",
                                        status_code=404)
