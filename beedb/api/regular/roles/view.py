class RoleView(object):

    role_attributes = [
        'name',
        'database_type',
        'created_at',
        'tenant',
        'updated_at',
        'description',
        'status',
        'password'
    ]

    def __init__(self, role_repr):
        self.data = role_repr

    def build_single_view(self, data):
        view = {'role': {}}
        for attr in self.role_attributes:
            view['role'].update({attr: data.get(attr)})
        return view

    def view(self):
        return self.build_single_view(self.data)


class RolesView(RoleView):

    def __init__(self, roles):
        super(RolesView, self).__init__(roles)

    def view(self):
        view = {'roles': []}
        for chunk in self.data:
            view['roles'].append(self.build_single_view(
                chunk)['role'])
        return view
