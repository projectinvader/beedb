"""empty message

Revision ID: 21321c9a9125
Revises: None
Create Date: 2015-09-15 16:32:28.233493

"""

# revision identifiers, used by Alembic.
revision = '21321c9a9125'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.create_table(
        'tenant_role',
        sa.Column('tenant', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('secret_reference', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=True),
        sa.Column('created_at', sa.String()),
        sa.Column('updated_at', sa.String()),
        sa.Column('status', sa.String(), nullable=False),
        sa.Column('database_type', sa.String(), nullable=False),
        sa.Column('id', sa.String(), nullable=False),
        sa.Column('database_type', sa.String(),
                  sa.ForeignKey('database_type.name'), nullable=False),
        sa.UniqueConstraint('name'),
        sa.PrimaryKeyConstraint('name'),
    )

    op.create_table(
        'database_type',
        sa.Column('id', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('description', sa.String(), nullable=True),
        sa.Column('username', sa.String(), nullable=False),
        sa.Column('password', sa.String(), nullable=False),
        sa.Column('database', sa.String(), nullable=False),
        sa.Column('host', sa.String(), nullable=False),
        sa.Column('port', sa.String(), nullable=False),
        sa.Column('created_at', sa.String()),
        sa.Column('updated_at', sa.String()),
        sa.Column('driver', sa.String(), nullable=False),
        sa.UniqueConstraint('name'),
        sa.PrimaryKeyConstraint('name'),
    )

    op.create_table(
        'tenant_database',
        sa.Column('id', sa.String(), nullable=False),
        sa.Column('tenant', sa.String(), nullable=True),
        sa.Column('name', sa.String(), nullable=True),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('created_at', sa.String()),
        sa.Column('updated_at', sa.String()),
        sa.Column('status', sa.String(), nullable=False),
        sa.Column('database_type', sa.String(),
                  sa.ForeignKey('database_type.name'), nullable=False),
        sa.Column('role', sa.String(), sa.ForeignKey('tenant_role.name'),
                  nullable=False),
        sa.UniqueConstraint('name')
    )


def downgrade():

    op.drop_table('database_type')
    op.drop_table('tenant_database')
    op.drop_table('tenant_role')
