========================================
BeeDB. Pure DBaaS for Cloud environments
========================================

--------
Overview
--------

BeeDB - formal DBaaS, except the need to deploy VMs in clouds.
BeeDB provides an API to provision and manage databases on shared clusters.
BeeDB is an extensible framework designed to work with different cloud/non-cloud
environments and different types of databases that does have pre-defined multi-tenancy mechanisms.

BeeDB maps cloud/non-cloud identity service accounts to database multi-tenancy model to
allow advanced database resource utilization.

-------------
How it works?
-------------

When user wants to create database, he uses BeeDB client to provision database within supported types.
User provides information that is necessary for authorization and if BeeDB client able to authorize user
it builds specific HTTP headers that are necessary for server-side authorization (including auth platform identifier).

Using unique user identifier BeeDB creates role and databases according to given user demands.
Here's how workflow looks like::

       At client side
       +-------------------------------------------------------------+
       | User --> BeeDB client --> Auth platform --> BeeDB client -->|
       +-------------------------------------------------------------+

       From client side to server side
       +---------------------------------------------------------------------------------+
       |--> BeeDB --> Auth platform --> BeeDB --> Secret Store --> Database --> BeeDB -->|
       +---------------------------------------------------------------------------------+

       From server side to client side
       +-------------------------+
       |--> BeeDB client --> User|
       +-------------------------+

----------------------------
What is currently supported?
----------------------------

Auth platforms::

    OpenStack
    vCloud Air
    AWS

Databases::

    PostgreSQL 9.4 (initial alpha version)

Secret store::

    HashiCorp Vault
    Plain (placebo, insecure)

--------------
How to deploy?
--------------

This is what necessary to run BeeDB server part::

    git clone git clone git@bitbucket.org:projectinvader/beedb.git
    cd beedb
    virtualenv .venv
    source .venv/bin/activate
    pip install -r requirements.txt -r test-requirements.txt
    python setup.py install

Then you'll be able to use next CLI tools to run and manage BeeDB::

    beedb-api - executable for BeeDB itself
    beedb-manage - executable to run management commands like create backend for BeeDB
    beedb-mgmt-api (currently not functioning)

-----------
How to run?
-----------

This is what you need to prepare your environment::

    Prepare configuration file that'll be used to start BeeDB and for management needs,
      as an example it is suggest to use sample config file from 'etc' folder

    Run 'beedb-manage' CLI tool
      $ beedb-manage --config-file etc/beedb/beedb-api.conf db upgrade

Also you would need to create set of database types that will be available for user::

    $ beedb-manage database-types create \
        --name postgresql-cluster-east \
        --driver beedb.storage.postgresql.PostgreSQLStorageV1 \
        --dba-username postgres \
        --dba-password postgres \
        --dba-db postgres \
        --db-host 10.23.12.221 \
        --db-port 5432

        +-------------+----------------------------------------------+
        | Property    | Value                                        |
        +-------------+----------------------------------------------+
        | created_at  | 2015-10-21 10:36:53.309074                   |
        | database    | postgres                                     |
        | description | None                                         |
        | driver      | beedb.storage.postgresql.PostgreSQLStorageV1 |
        | host        | 10.23.12.221                                 |
        | name        | postgresql-cluster-east                      |
        | password    | postgres                                     |
        | port        | 5432                                         |
        | update_at   | 2015-10-21 10:36:53.309081                   |
        | username    | postgres                                     |
        +-------------+----------------------------------------------+

Use 'list' to see available database types::

    $ beedb-manage database-types list

        +-------------+----------------------------------------------+
        | Property    | Value                                        |
        +-------------+----------------------------------------------+
        | created_at  | 2015-10-21 10:36:53.309074                   |
        | database    | postgres                                     |
        | description | None                                         |
        | driver      | beedb.storage.postgresql.PostgreSQLStorageV1 |
        | host        | 10.23.12.221                                 |
        | name        | postgresql-cluster-east                      |
        | password    | postgres                                     |
        | port        | 5432                                         |
        | update_at   | 2015-10-21 10:36:53.309081                   |
        | username    | postgres                                     |
        +-------------+----------------------------------------------+

Use 'delete' to drop a database type by its name::

    $ beedb-manage database-types delete --name postgresql-cluster-east
        OK. Done.

This is what you need to run BeeDB::

    $ beedb-api --config-file etc/beedb/beedb-api.conf

Now you have running BeeDB, use it!

-----------------
How to run tests?
-----------------

This section will explain how to run tests for BeeDB.
First of all, in order to test PostgreSQL driver you need to have reachable PostgreSQL server.
Current tests are expecting next environment variables::

    TEST_POSTGRESQL_SUPERUSER
    TEST_POSTGRESQL_DATABASE
    TEST_POSTGRESQL_HOST
    TEST_POSTGRESQL_SUPERUSER_PASSWD
    TEST_POSTGRESQL_PORT
    TEST_SECRET_STORE_PLATFORM
    TEST_VAULT_URL
    TEST_VAULT_ROOT_TOKEN


This variables are necessary to build admin connector to PostgreSQL.
BeeDB follows best practices in Python development, for running tests you need to do next steps::

    pip install -r test-requirements.txt

To run PEP8 style checks::

    tox -e pep8

To run integration tests::

    export TEST_POSTGRESQL_SUPERUSER=postgres
    export TEST_POSTGRESQL_DATABASE=postgres
    export TEST_POSTGRESQL_HOST=10.23.12.221
    export TEST_POSTGRESQL_SUPERUSER_PASSWD=postgres
    export TEST_POSTGRESQL_PORT=5432
    export TEST_SECRET_STORE_PLATFORM="beedb.secrets.vault.SecretStore"
    export TEST_VAULT_URL=http://192.168.0.111:8200
    export TEST_VAULT_ROOT_TOKEN=bb885248-b5d4-b56f-7a45-b0e8eb5f1c4b

    tox -e postgresql-py27

